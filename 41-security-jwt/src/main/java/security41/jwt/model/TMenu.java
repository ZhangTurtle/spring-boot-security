package security41.jwt.model;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;

@Data
public class TMenu implements GrantedAuthority {

    private Long id;

    private Long pId;

    private String name;

    private String url;

    private String authority;

    private Integer sort;

    private List<TRole> roleList = new ArrayList<>();

}
