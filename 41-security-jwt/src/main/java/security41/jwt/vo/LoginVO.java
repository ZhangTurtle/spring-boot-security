package security41.jwt.vo;

import lombok.Data;

@Data
public class LoginVO {

    private String username;

    private String password;

}
